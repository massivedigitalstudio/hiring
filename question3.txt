Imagine you have this code:
var a = [1, 2, 3]

- Will this result in a crash?  a[10] = 99;
- What will this output? console.log(a[6]);


